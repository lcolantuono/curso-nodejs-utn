var express = require('express');
var app = express();
var body_parser = require('body-parser');
var http = require("axios");
const api_key = "aa2f22898bbdcbbfd803e950e850fed4";
app.use(body_parser.json());

app.get('/', function(request, response) {
	response.send('ok');
});

app.listen(process.env.PORT || 3000, () => {
	console.log('Iniciando la aplicación en http://localhost:3000');
});

var contactos = [
	{
        id: 1,
		"nombre": "Juan",
        "apellido": "Perez",
        "telefono": "11223344",
        "email": "mi_contacto@gmail.com",
        "ciudad": "Remedios de Escalada",
        "provincia": "Buenos Aires",
        "pais": "Argentina"
    },
	{
        id: 2,
		"nombre": "Lucas",
        "apellido": "Colantuono",
        "telefono": "999999",
        "email": "colantuonolucas@gmail.com",
        "ciudad": "Cipolletti",
        "provincia": "Rio Negro",
        "pais": "Argentina"
    }
];

// GET /contactos => obtiene el listado de contactos
app.get('/contactos', (req, res) => {
	const respuesta = {
		status: 'ok',
		items: contactos
	}
	res.status(200).json(respuesta);
});

// POST /contactos => Registra un nuevo contacto en el sistema
app.post('/contactos', (req, res) => {
    var new_contacto = req.body;
    new_contacto.id = contactos.length + 1;
    contactos.push(new_contacto);
    const respuesta = {
        status: 'ok',
        item: new_contacto
    }
    res.status(200).json(respuesta);
});

function getClima(nombre_ciudad) {
    http.get("https://api.openweathermap.org/data/2.5/weather?APPID=" + api_key + "&units=metric&q=" + nombre_ciudad).then(respuesta => {
        console.log(respuesta);
        return respuesta.data.main.temp;
    }).catch(error => {
        console.log(error);
    });
}

// GET /contactos/{id} => Contacto con id por param
app.get('/contactos/:id', (req, res) => {
    const id = req.params.id;
    if (id <= contactos.length) {
        const respuesta = {
            status: 'ok',
            item: contactos[id - 1]
        }
        http.get("https://api.openweathermap.org/data/2.5/weather?APPID=" + api_key + "&units=metric&q=" + respuesta.item.ciudad).then(respuesta_clima => {
            respuesta.item.temperatura = respuesta_clima.data.main.temp;
        }).catch(error => {
            console.log(error);
        });
        res.status(200).json(respuesta);
    } else {
        const respuesta = {
            status: 'error',
            message: 'Not found'
        }
        res.status(404).json(respuesta);
    }
});

// PUT /contactos/{id} => Actualiza los datos registrados de un contacto
app.put('/contactos/:id', (req, res) => {
    const id = req.params.id;
    if (id <= contactos.length) {
        const edit_contacto = contactos[id - 1];
        edit_contacto.nombre = req.body.nombre;
        edit_contacto.apellido = req.body.apellido;
        edit_contacto.email = req.body.email;
        edit_contacto.ciudad = req.body.ciudad;
        edit_contacto.provincia = req.body.provincia;
        edit_contacto.pais = req.body.pais;
        contactos[id - 1] = edit_contacto;
        const respuesta = {
            status: 'ok',
            item: edit_contacto
        }
        res.status(200).json(respuesta)
    } else {
        const respuesta = {
            status: 'error',
            message: 'Not found'
        }
        res.status(404).json(respuesta);
    }
});

// DELETE /contactos/{id} => Borra los datos registrados de un contacto
app.delete('/contactos/:id', (req, res) => {
    const id = req.params.id;
    if (id <= contactos.length) {
        contactos.splice(id - 1, 1);
        const respuesta = {
            status: 'ok'
        }
        res.status(200).json(respuesta);
    } else {
        const respuesta = {
            status: 'error',
            message: 'Not found'
        }
        res.status(404).json(respuesta);
    }
});
